//
//  main.cpp
//  IntList
//
//  Created by Yevon Zhang on 17/2/14.
//  Copyright (c) 2017年 Yevon Zhang. All rights reserved.
//

#include <iostream>
#include "IntList.h"

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Your Student No. and Name\n";
    IntList a(5,1);
    std::cout << "a(5,1)\n";
    a.print();
    
    a.resize(6,2);
    std::cout << "a.resize(6,2)\n";
    a.print();
    
    a.resize(4,2);
    std::cout << "a.resize(4,2)\n";
    a.print();
    
    IntList b;
    std::cout << "b\n";
    b.print();
    
    a=b;
    std::cout << "a=b\n";
    a.print();
    b.print();
    
    b.push_back(2);
    std::cout << "b.push_back(2)\n";
    a.print();
    b.print();
    
    b=b;
    std::cout << "b=b\n";
    a.print();
    b.print();
    
    b=a;
    std::cout << "b=a\n";
    a.print();
    b.print();
    
    b.push_back(2);
    std::cout << "b.push_back(2)\n";
    a.print();
    b.print();
    
    return 0;
}
