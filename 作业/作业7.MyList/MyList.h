//
//  MyList.h
//  MyList
//
//  Created by Yevon Zhang on 17/2/14.
//  Copyright (c) 2017年 Yevon Zhang. All rights reserved.
//

#ifndef __MyList__MyList__
#define __MyList__MyList__

#include <stdio.h>
#include <ostream>
#include <istream>
#include <stdexcept>

class EOFException:public std::runtime_error{
    
};

template <typename T>
class MyList {

public:
    //function members
    MyList(int numVals = 0, T val = T()); // constructor
    MyList(const MyList &il); // copy constructor
    ~MyList(); // destructor
    MyList& operator=(const MyList &il); // = operator
    int size() const; // size of the list
    int getCapacity() const;//capacity of the list
    // resize list
    //当n大于原来的size，使用val初始化多出的元素
    //当n小于原谅的size，直接截断原list
    void resize(int n = 0, T val = T());// resize list
    //reserver the capacity of size,if required capacity is less than size,do nothing
    void reserve(int capacity);
    void push_back(T val); // add new last element
    //print the list
    //输出格式  列表长度：元素1 元素2 元素3 ...
    void print();//print the list
    //取出index所在位置的整形数
    T& operator [] ( const int index) throw (std::runtime_error);
    //取出index所在位置的整形数
    const T& operator [] ( const int index) const throw ( EOFException );

private:
    // data members
    T* values; //pointer to elements
    int numberValues; //size of list
    int capacity;
};
//判断参数中的两个MyList是否相等，相等返回true，返回false
//相等定义为list的类型、长度和list中的每个数据都相等
template<typename T>
bool operator == (const MyList<T>& il1, const MyList<T>& il2);
//判断参数中的两个MyList是否相等，相等返回false，不等返回true
//相等定义为list类型、长度和list中的每个数据都相等
template<typename T>
bool operator != (const MyList<T>& il1, const MyList<T>& il2);
//参数中的两个MyList相加
//两个list不等长的情况下，短list中差的元素视为0
//如果两个list的类型不同，涉及char、string类型的不予处理，输出“类型不匹配”，返回il1
//涉及int，float，double，number类型的对其实部进行计算，应考虑
template<typename T>
MyList<T> operator + (const MyList<T>& il1, const MyList<T>& il2);
//参数中的两个MyList相减，il1-il2
//两个list不等长的情况下，短list中差的元素视为0
//如果两个list的类型不同，涉及char、string类型的不予处理，输出“类型不匹配”，返回il1
//涉及int，float，double，number类型的对其实部进行计算
template<typename T>
MyList<T> operator - (const MyList<T>& il1, const MyList<T>& il2);
//输出il，支持文件输出，调用T自己的输出运算符重载函数
template<typename T>
std::ostream& operator << (std::ostream& ost, const MyList<T>& il);
//由用户输入il的内容，支持文件输入，调用T自己的输出运算符重载函数
template<typename T>
std::istream& operator >> (std::istream& ist, MyList<T>& il);

#endif /* defined(__MyList__MyList__) */
