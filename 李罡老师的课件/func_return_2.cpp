#include <iostream>
using namespace std;

class A{
public:
  A(){ cout << "A's default constructor" <<endl;}
  A(const A& a){ cout << "A's copy constructor" <<endl; }
  string toString() const {return this->s; }
private:
  string s = "hello";
};

A g;

//返回非引用: 
A f1() { return A();} 
A f1x() { A a; return a;}
//返回常非引用:
const A f2() { return A();}
const A f2x() { A a; return a;}
//返回非函数内局部变量的引用: 
A& f3() { return g;}
//返回非函数内局部变量的常引用:
const A& f4() { return g;}
//返回函数内局部变量的引用:
A& f5() { A a; return a;}
//返回函数内局部变量的常引用:
const A& f6() { A a; return a;}

int main(){

  cout << "===第1列，返回非引用===" << endl;
  f1();

  cout << "返回非引用，用非引用存放返回值。" << endl;
  A a1 = f1();
  cout<<a1.toString()<<endl;

  cout << "返回非引用，用常非引用存放返回值。" << endl;
  const A b1 = f1();
  cout<<b1.toString()<<endl;

  cout << "返回非引用，用引用存放返回值。" << endl;
  //A& c1 = f1();
  //cout<<c1.toString()<<endl;

  cout << "返回非引用，用常引用存放返回值。" << endl;
  const A& d1 = f1();
  cout<<d1.toString()<<endl;

  cout << "===第2列，返回常非引用===" << endl;
  f2();

  cout << "返回常非引用，用非引用存放返回值。" << endl;
  A a2 = f2();
  cout<<a2.toString()<<endl;

  cout << "返回常非引用，用常非引用存放返回值。" << endl;
  const A b2 = f2();
  cout<<b2.toString()<<endl;

  cout << "返回常非引用，用引用存放返回值。" << endl;
  //A& c2 = f2();
  //cout<<c2.toString()<<endl;

  cout << "返回常非引用，用常引用存放返回值。" << endl;
  const A& d2 = f2();
  cout<<d2.toString()<<endl;


  cout << "===第3列，返回非函数内局部变量的引用===" << endl;
  f3();

  cout << "返回非函数内局部变量的引用，用非引用存放返回值。" << endl;
  A a3 = f3();
  cout<<a3.toString()<<endl;

  cout << "返回非函数内局部变量的引用，用常非引用存放返回值。" << endl;
  const A b3 = f3();
  cout<<b3.toString()<<endl;

  cout << "返回非函数内局部变量的引用，用引用存放返回值。" << endl;
  A& c3 = f3();
  cout<<c3.toString()<<endl;

  cout << "返回非函数内局部变量的引用，用常引用存放返回值。" << endl;
  const A& d3 = f3();
  cout<<d3.toString()<<endl;

  cout << "===第4列，返回非函数内局部变量的常引用===" << endl;
  f4();

  cout << "返回非函数内局部变量的常引用，用非引用存放返回值。" << endl;
  A a4 = f4();
  cout<<a4.toString()<<endl;

  cout << "返回非函数内局部变量的常引用，用常非引用存放返回值。" << endl;
  const A b4 = f4();
  cout<<b4.toString()<<endl;

  cout << "返回非函数内局部变量的常引用，用引用存放返回值。" << endl;
  //A& c4 = f4();
  //cout<<c4.toString()<<endl;

  cout << "返回非函数内局部变量的常引用，用常引用存放返回值。" << endl;
  const A& d4 = f4();
  cout<<d4.toString()<<endl;

  cout << "===第5列，返回函数内局部变量的引用===" << endl;
  f5();

  cout << "返回函数内局部变量的引用，用非引用存放返回值。" << endl;
  A a5 = f5();
  cout<<a5.toString()<<endl;

  cout << "返回函数内局部变量的引用，用常非引用存放返回值。" << endl;
  const A b5 = f5();
  cout<<b5.toString()<<endl;

  cout << "返回函数内局部变量的引用，用引用存放返回值。" << endl;
  A& c5 = f5();
  cout<<c5.toString()<<endl;

  cout << "返回函数内局部变量的引用，用常引用存放返回值。" << endl;
  const A& d5 = f5();
  cout<<d5.toString()<<endl;

  cout << "===第6列，返回函数内局部变量的常引用===" << endl;
  f6();

  cout << "返回函数内局部变量的常引用，用非引用存放返回值。" << endl;
  A a6 = f6();
  cout<<a6.toString()<<endl;

  cout << "返回函数内局部变量的常引用，用常非引用存放返回值。" << endl;
  const A b6 = f6();
  cout<<b6.toString()<<endl;

  cout << "返回函数内局部变量的常引用，用引用存放返回值。" << endl;
  //A& c6 = f6();
  //cout<<c6.toString()<<endl;

  cout << "返回函数内局部变量的常引用，用常引用存放返回值。" << endl;
  const A& d6 = f6();
  cout<<d6.toString()<<endl;

  return 0;
}
