#include <iostream>

using namespace std;

class Memb1
{
// member data
public:
    Memb1 ( )
    {
        cout << "Memb1 con\n";
    }
    ~ Memb1 ( )
    {
        cout << "Memb1 dest\n";
    }
};
class Memb2
{
// member data
public:
    Memb2 ( )
    {
        cout << "Memb2 con\n";
    }
    ~ Memb2 ( )
    {
        cout << "Memb2 dest\n";
    }
};
class Memb3
{
// member data
public:
    Memb3 ( )
    {
        cout << "Memb3 con\n";
    }
    ~ Memb3 ( )
    {
        cout << "Memb3 dest\n";
    }
};
class Memb4
{
// member data
public:
    Memb4 ( )
    {
        cout << "Memb4 con\n";
    }
    ~ Memb4 ( )
    {
        cout << "Memb4 dest\n";
    }
};

class Base
{
public:
    Base( )
    {
        cout << "Base con\n";
    }
    Base(int x)
    {
        cout << "Base con " << x << endl;
    }
    ~Base( )
    {
        cout << "Base dest\n";
    }
};
class Derived1 : public Base
{
public:
    Derived1 ( )
    {
        cout << "Derived1 con\n";
    }
    ~ Derived1 ( )
    {
        cout << "Derived1 dest\n";
    }
private:
    Memb1 m1;
    Memb2 m2;
};

class Derived2 : public Derived1
{
public:
    Derived2 ( )
    {
        cout << "Derived2 con\n";
    }
    ~ Derived2 ( )
    {
        cout << "Derived2 dest\n";
    }
private:
    Memb3 m3;
    Memb4 m4;
};

int main()
{
    Derived2 x;
    return 0;
}
